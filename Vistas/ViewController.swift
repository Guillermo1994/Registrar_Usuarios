//
//  ViewController.swift
//  Vistas
//
//  Created by Guillermo Garcia Romero on 27/04/17.
//  Copyright © 2017 Guillermo Garcia Romero. All rights reserved.
//

import UIKit
import Material

class ViewController: UIViewController {

    @IBOutlet var Nombre: TextField!
    @IBOutlet var Apellido: TextField!
    @IBOutlet var FechaNacimiento: TextField!
    @IBOutlet var Genero: TextField!
    @IBOutlet var Pais: TextField!
    @IBOutlet var Email: TextField!
    @IBOutlet var Password: TextField!
    @IBOutlet var ConfirmarPassword: TextField!
    @IBOutlet var Foto: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        Nombre.placeholder="Nombre(s)"
        Apellido.placeholder="Apellido(s)"
        FechaNacimiento.placeholder="Fecha de Nacimiento"
        Genero.placeholder="Genero"
        Pais.placeholder="Pais"
        Email.placeholder="Email"
        Password.placeholder="Password"
        ConfirmarPassword.placeholder="Confirmar Password"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func boton(_ sender: Any) {
        print("El dispositivo es \(UIDevice.current.model)")
    }
    


}

