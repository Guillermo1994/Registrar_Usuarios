//
//  VCNuevo.swift
//  Vistas
//
//  Created by Guillermo Garcia Romero on 19/05/17.
//  Copyright © 2017 Guillermo Garcia Romero. All rights reserved.
//

import UIKit
import Firebase

let Paises=["México","Estados Unidos","Canada"]
let Generos=["Masculino","Femenino"]

var firstName=String()
var lastName=String()
var birthDate = String()
var gender = String()
var country = String()
var email=String()
var password=String()



let info_Usuario = ["firstName": firstName,
                    "lastName" : lastName,
                    "birthDate":birthDate,
                    "gender":gender,
                    "country":country,
                    "email":email,
                    "password":password]



class VCNuevo: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    
    @IBOutlet var Picker_Pais: UIPickerView!
    @IBOutlet var Textf_Pais: UITextField!
    @IBOutlet var Picker_Genero: UIPickerView!
    @IBOutlet var Textf_Genero: UITextField!
    @IBOutlet var Picker_Date: UIDatePicker!
    @IBOutlet var Textf_Fecha_Nacimiento: UITextField!
    
    @IBOutlet var Textf_Nombre: UITextField!
    @IBOutlet var Textf_Apellido: UITextField!
    
    @IBOutlet var Textf_Password_Confirmacion: UITextField!
    @IBOutlet var Textf_Password: UITextField!
    @IBOutlet var Textf_Email: UITextField!
    
    var ref:FIRDatabaseReference!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        Picker_Date.backgroundColor=UIColor.white
       self.ref = FIRDatabase.database().reference()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
        
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if (pickerView == Picker_Pais){
            return Paises.count
        }else if  pickerView==Picker_Genero{
            
            return Generos.count
            
        }else{
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == Picker_Pais  {
            self.view.endEditing(true)
            return Paises[row]
        }else if   pickerView == Picker_Genero{
            self.view.endEditing(true)
            return Generos[row]
        }else{
            return ""
        }
        
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == Picker_Genero {
            self.Textf_Genero.text = Generos[row]
            self.Picker_Genero.isHidden = true
        }else if pickerView == Picker_Pais{
            self.Textf_Pais.text = Paises[row]
            self.Picker_Pais.isHidden = true
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        
        if pickerView == Picker_Pais {
            pickerLabel.text = Paises[row]
        }else if pickerView == Picker_Genero{
            pickerLabel.text = Generos[row]
        }
        
        
        pickerLabel.font=UIFont(name:"Helvetica", size: 30)
        pickerLabel.textAlignment = .left
        
        return pickerLabel
        
        
    }
    
   
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.Textf_Pais {
            self.Picker_Pais.isHidden = false
            //if you dont want the users to se the keyboard type:
            
            textField.endEditing(true)
        }else if textField == self.Textf_Genero{
            self.Picker_Genero.isHidden = false
            //if you dont want the users to se the keyboard type:
            
            textField.endEditing(true)
        }else if textField == self.Textf_Fecha_Nacimiento{
            self.Picker_Date.isHidden = false
            textField.endEditing(true)
        }
        
    }
    
    @IBAction func Seleccionar_Fecha(_ sender: Any) {
        
        let dateformat = DateFormatter()
        dateformat.dateFormat = "MM/dd/YYYY"
        
        Textf_Fecha_Nacimiento.text=dateformat.string(from: Picker_Date.date)
        Picker_Date.isHidden=true
    }
    
    @IBAction func Agregar_Usuario(_ sender: UIButton) {
        if (Textf_Fecha_Nacimiento.text?.isEmpty)! && (Textf_Nombre.text?.isEmpty)! && (Textf_Apellido.text?.isEmpty)! && (Textf_Pais.text?.isEmpty)! && (Textf_Genero.text?.isEmpty)! && (Textf_Email.text?.isEmpty)! && (Textf_Password.text?.isEmpty)! && (Textf_Password_Confirmacion.text?.isEmpty)!{
            
            print("error campos")
            
        }else if (Textf_Password_Confirmacion.text != Textf_Password.text){
            
        print("error password")
        
        }else{
            firstName = Textf_Nombre.text!
            lastName = Textf_Apellido.text!
            birthDate = Textf_Fecha_Nacimiento.text!
            gender = Textf_Genero.text!
            country = Textf_Pais.text!
            password = Textf_Password.text!
            email = Textf_Email.text!
            
            self.ref.childByAutoId().setValue(info_Usuario)
            print("Se agrego el usuario")
        }
        
        
        
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}
